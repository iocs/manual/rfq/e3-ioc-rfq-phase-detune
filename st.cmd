require essioc

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

dbLoadRecords("$(TOP)/db/rfq_phase_detune.db", "P=RFQ-010::")

# Call iocInit to start the IOC
iocInit()
date
